import unittest
import maths


class TestMaths(unittest.TestCase):
    def test_carre(self):
        self.assertEqual(maths.mettre_au_carre(2), 4)

    def test_cube(self):
        self.assertEqual(maths.mettre_au_cube(2), 8)


if __name__ == "__main__":
    unittest.main()
