def mettre_au_carre(x: int) -> int:
    return x * x


def mettre_au_cube(x: int) -> int:
    return x * (x * x)
