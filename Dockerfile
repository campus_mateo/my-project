# Set the base image for subsequent instructions
FROM php:8.2

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install -qq git curl libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev libzip-dev nodejs npm

# Clear out the local repository of retrieved package files
RUN apt-get clean

# Install needed extensions
# Note: mcrypt is removed since it's deprecated
RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install gd pdo_mysql zip

# Install Composer
RUN curl --silent --show-error "https://getcomposer.org/installer" | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~1.0"


# Ensure global composer bin is in the PATH
ENV PATH="/root/.composer/vendor/bin:${PATH}"
