#!/usr/bin/env bash

# Aller dans le répertoire backend et lancer le serveur en arrière-plan
cd ../backend || exit
php artisan serve &

# Enregistrer le PID du serveur
SERVER_PID=$!

# Attendre un moment pour s'assurer que le serveur est démarré
sleep 10

# Aller dans le répertoire frontend et exécuter les tests
cd .. || exit
cd frontend || exit
ls -al
cat package.json
npm install
npm run build
npm run test:e2e

# Terminer le serveur après les tests
kill $SERVER_PID